from setuptools import setup


with open('requirements.txt') as req:
    install_requires = req.read().split()


setup(
    name='sagittariidae',
    packages=[
        'sagittariidae',
        'sagittariidae.alembic',
    ],
    install_requires=install_requires,
    extra_require={
        'linting': [
            'mccabe',
            'pycodestyle',
            'pydocstyle',
            'yapf',
            'pyflakes',
        ]
    },
)
