Required Dependencies
=======================
To run the Sagittariidae back-end, the following dependencies are required:
1. `python3`
2. `pipenv`
3. `sqlite`

Configuring/Running the Project
===============================
To configure, run the following commands:
```
$ pipenv install -e .
$ pipenv shell
$ alembic upgrade head
```

Now the back-end can be run from within the virtual environment with:
```
$ flask run
```
