
import logging
import time

from flask import Flask, session
from flask_sqlalchemy import SQLAlchemy

from flask_sso import SSO

from config import STATIC_ROOT, STORE_PATH

app = Flask(__name__, static_folder=STATIC_ROOT, static_url_path='')
db = SQLAlchemy(app)

app.config.from_object('config')
isdevmode = app.config['TESTING'] or app.config['DEBUG']

# Please use a sane timezone for log entries so that we don't have to jump
# through daylight savings hoops.
logging.Formatter.converter = time.gmtime
formatter = logging.Formatter('%(asctime)s [%(levelname)s] %(threadName)s,%(module)s,l%(lineno)d : %(message)s')

# Set up the level at which we want to log
if isdevmode:
    loglevel = logging.DEBUG
else:
    loglevel = logging.INFO

loggers = {app.logger                      : None,
           logging.getLogger('werkzeug')   : logging.INFO,
           logging.getLogger('sqlalchemy') : logging.WARN}
handlers = []

cons_log = logging.StreamHandler()
handlers.append(cons_log)

# Set up the log handlers.
if isdevmode:
    file_log = logging.FileHandler('sagittariidae.log')
    handlers.append(file_log)


# Assign handlers to loggers and set them all at the appropriate level
for handler in handlers:
    handler.setLevel(loglevel)
    handler.setFormatter(formatter)

    for logger, defined_level in loggers.items():
        if defined_level is None:
            logger.setLevel(loglevel)
        else:
            logger.setLevel(defined_level)
        logger.addHandler(handler)

try:
    logger.info('Sagittariidae is starting ...')

    # Load "leaf" modules.  These may depend only on `app.app` which has now
    # been initialised
    from . import models

    # Lead "middleware" modules.  These may depend on both leaf modules and on
    # `app.app`.
    from . import sampleresolver
    from . import views

    # And we're done
    logger.info('Sagittariidae is ready to serve.')
except Exception as e:
    logger.error('Startup error', exc_info=e)
    import sys
    sys.exit(1)


# SSO #########################################################################

app.config['SSO_ATTRIBUTE_MAP'] = {
    "HTTP_SHIB_IDENTITY_PROVIDER": (False, "idp"),
    "HTTP_SHIB_SHARED_TOKEN": (False, "shared_token"),
    "HTTP_SHIB_CN": (False, "cn"),
    "HTTP_SHIB_MAIL": (False, "email"),
    "HTTP_SHIB_GIVENNAME": (False, "first_name"),
    "HTTP_SHIB_SN": (False, "last_name"),
}

app.config['SSO_LOGIN_URL'] = '/login'
app.config['SSO_LOGIN_ENDPOINT'] = 'login'

sso = SSO(app=app)

@sso.login_handler
def login_callback(user_info):
    """Store user information in session"""
    session['user'] = user_info
