"""empty message

Revision ID: 235815242174
Revises: a10b578dbae3
Create Date: 2019-11-13 12:33:08.999491

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '235815242174'
down_revision = 'a10b578dbae3'
branch_labels = None
depends_on = None


def upgrade():
    op.drop_table('project_group')
    op.create_table('project_group',
    sa.Column('project_id', sa.Integer(), nullable=True),
    sa.Column('gid', sa.Integer(), nullable=False),
    sa.ForeignKeyConstraint(['project_id'], ['project.id'], ),
    sa.ForeignKeyConstraint(['gid'], ['group.id']),
    # sa.PrimaryKeyConstraint('gid', 'uid'),
    sa.UniqueConstraint('project_id', 'gid', name='_pid_gid_uc'),
    sa.Column('id', sa.Integer(), nullable=False, autoincrement=True, primary_key=True),
    sa.Column('obfuscated_id', sa.String(length=15), nullable=True),
    sa.UniqueConstraint('obfuscated_id'),
    sa.UniqueConstraint('id'),
    sa.Column('read', sa.Boolean(), nullable=False),
    sa.Column('write', sa.Boolean(), nullable=False)
    )


def downgrade():
    pass
