"""empty message

Revision ID: aa5f2dae531d
Revises: cf8c4a365057
Create Date: 2019-11-08 10:18:39.576758

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'aa5f2dae531d'
down_revision = 'cf8c4a365057'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    # op.add_column('group_user', sa.Column('id', sa.Integer(), nullable=False))
    # op.add_column('group_user', sa.Column('obfuscated_id', sa.String(length=15), nullable=True))
    op.drop_table('group_user')

    op.create_table('group_user',
    sa.Column('gid', sa.Integer(), nullable=False),
    sa.Column('uid', sa.Integer(), nullable=False),
    sa.ForeignKeyConstraint(['gid'], ['group.id'], ),
    sa.ForeignKeyConstraint(['uid'], ['user.id'], ),
    sa.PrimaryKeyConstraint('gid', 'uid'),
    sa.Column('id', sa.Integer, primary_key=False, autoincrement=True),
    sa.UniqueConstraint('gid', 'uid', name='_gid_uid_uc')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_constraint(None, 'user', type_='unique')
    op.drop_constraint(None, 'group_user', type_='unique')
    op.drop_constraint('_gid_uid_uc', 'group_user', type_='unique')
    op.drop_column('group_user', 'obfuscated_id')
    op.drop_column('group_user', 'id')
    # ### end Alembic commands ###
