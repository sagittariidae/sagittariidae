"""empty message

Revision ID: a10b578dbae3
Revises: f94b7093dfba
Create Date: 2019-11-13 10:35:56.408709

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'a10b578dbae3'
down_revision = 'f94b7093dfba'
branch_labels = None
depends_on = None


def upgrade():
    op.drop_table('project_user')
    op.create_table('project_user',
    sa.Column('project_id', sa.Integer(), nullable=True),
    sa.Column('uid', sa.Integer(), nullable=False),
    sa.ForeignKeyConstraint(['project_id'], ['project.id'], ),
    sa.ForeignKeyConstraint(['uid'], ['user.id']),
    # sa.PrimaryKeyConstraint('gid', 'uid'),
    sa.UniqueConstraint('project_id', 'uid', name='_pid_uid_uc'),
    sa.Column('id', sa.Integer(), nullable=False, autoincrement=True, primary_key=True),
    sa.Column('obfuscated_id', sa.String(length=15), nullable=True),
    sa.UniqueConstraint('obfuscated_id'),
    sa.UniqueConstraint('id'),
    sa.Column('read', sa.Boolean(), nullable=False),
    sa.Column('write', sa.Boolean(), nullable=False)
    )


def downgrade():
    pass
