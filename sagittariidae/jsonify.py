'''module to jsonify our models'''
import re
import os
from urllib.parse import quote

from flask.json import dumps, JSONEncoder

from .app import app, db, models


__all__ = ['jsonize']


def jsonify(*args, **kwargs):
    indent = None
    separators = (",", ":")

    if app.config["JSONIFY_PRETTYPRINT_REGULAR"] or app.debug:
        indent = 2
        separators = (", ", ": ")

    if args and kwargs:
        raise TypeError("jsonify() behavior undefined when passed both args and kwargs")
    elif len(args) == 1:  # single args are passed directly to dumps()
        data = args[0]
    else:
        data = args or kwargs

    return app.response_class(
        dumps(data, cls=DBModelJSONEncoder, indent=indent, separators=separators) + "\n",
        mimetype=app.config["JSONIFY_MIMETYPE"],
    )


class DBModelJSONEncoder(JSONEncoder):

    BAD_URI_PAT  = re.compile("%.{2}|\/|_")
    COLLAPSE_PAT = re.compile("-{2,}")

    def __init__(self, **kw):
        super(DBModelJSONEncoder, self).__init__(**kw)

        # This is a terrible hack to make it possible to add context to the
        # stages IDs.  The relative ordering of stages is important, and from
        # this we infer the ordinal position values.  Note this that assumes
        # that a new encoder instance is used for each JSON document; do not
        # cache encoders!
        self.sample_stage_count = 0

    def _uri_name(self, obfuscated_id, name):
        """
        Convert the name of a resource (like a project or sample) into a
        URI-friendly form.  This function has strong opinions about what a
        "good" URI ID is, and will complain if the ID cannot be rendered in
        an acceptable form.  In particular, it should not contain any
        characters that need to be escaped; cf. `urllib.quote()`.
        """
        assert obfuscated_id is not None
        assert name is not None
        # URI-escape special characters
        new_name = quote(name.lower().replace(" ", "-"))
        # Convert escapes into underscores; i.e. foo%20bar -> foo-bar
        new_name = re.sub(self.BAD_URI_PAT, '-', new_name)
        # Collapse multiple consecutive hyphens; i.e. foo---bar -> foo-bar
        new_name = re.sub(self.COLLAPSE_PAT, '-', new_name)
        # concat with the ID and return
        return '-'.join([obfuscated_id, new_name])

    def _dictify(self, model, exclude={}):
        """
        Return a model as a dictionary. 'Private' attributes are removed and
        underscores are replaced with more hyphens in attribute names, making
        for more aesthetically pleasing HTTP data maps.
        """
        def tr(kv):
            return (kv[0].replace('_', '-'), kv[1])
        return dict(tr(kv) for kv in iter(list(model.__dict__.items()))
                    if (not (kv[0].startswith('_') or (kv[0] in exclude))))

    def strip_private_fields(self, d):
        del d['obfuscated-id']
        return d

    def _encodeProject(self, p):
        d = self._dictify(p)
        d['id'] = self._uri_name(d['obfuscated-id'], d['name'])
        return self.strip_private_fields(d)

    def _encodeMethod(self, m):
        d = self._dictify(m)
        d['id'] = self._uri_name(d['obfuscated-id'], d['name'])
        return self.strip_private_fields(d)

    def _encodeSample(self, s):
        d = self._dictify(s, {'project'})
        d['id'] = self._uri_name(d['obfuscated-id'], d['name'])
        d['project'] = self._uri_name(s.project.obfuscated_id, s.project.name)
        return self.strip_private_fields(d)
    def _encodeProjectUser(self, s):
        d = self._dictify(s, {'project'})
        # d['id'] = self._uri_name(d['obfuscated-id'], d['name'])
        d['project'] = self._uri_name(s.project.obfuscated_id, s.project.name)
        return self.strip_private_fields(d)
    def _encodeProjectGroup(self, s):
        d = self._dictify(s, {'project'})
        # d['id'] = self._uri_name(d['obfuscated-id'], d['name'])
        d['project'] = self._uri_name(s.project.obfuscated_id, s.project.name)
        return self.strip_private_fields(d)

    def _encodeSampleStage(self, ss):
        d = self._dictify(ss, {'sample', 'method'})
        self.sample_stage_count += 1
        d['id'] = self._uri_name(d['obfuscated-id'], str(self.sample_stage_count))
        d['sample'] = self._uri_name(ss.sample.obfuscated_id, ss.sample.name)
        d['method'] = self._uri_name(ss.method.obfuscated_id, ss.method.name)
        return self.strip_private_fields(d)

    def _encodeSampleStageFile(self, ssf):
        def isready(ssf):
            return ssf.status in [models.FileStatus.archived,
                                  models.FileStatus.cleaned,
                                  models.FileStatus.complete]
        if isready(ssf):
            status = 'ready'
            fname = os.path.basename(ssf.relative_target_path)
        else:
            status = 'processing'
            fname = os.path.basename(ssf.relative_target_path)
        return {'id'           : ssf.obfuscated_id + '-' + fname,
                'file'         : fname,
                'status'       : status,
                'mtime'        : ssf.modified_ts.isoformat(),
                'uri' : '/dl/' + ssf.relative_target_path}

    def _encodeModel(self, m):
        return self.strip_private_fields(self._dictify(m))

    def default(self, thing):
        if isinstance(thing, models.Project):
            return self._encodeProject(thing)
        if isinstance(thing, models.Method):
            return self._encodeMethod(thing)
        if isinstance(thing, models.Sample):
            return self._encodeSample(thing)
        if isinstance(thing, models.SampleStage):
            return self._encodeSampleStage(thing)
        if isinstance(thing, models.SampleStageFile):
            return self._encodeSampleStageFile(thing)
        if isinstance(thing, models.Project_user):
            return self._encodeProjectUser(thing)
        if isinstance(thing, models.Project_group):
            return self._encodeProjectGroup(thing)
        if isinstance(thing, db.Model):
            return self._encodeModel(thing)
        return super(DBModelJSONEncdore, self).default(thing)
