import io
import glob
import hashlib
import json
import math
import os
import re

from flask          import abort, jsonify, redirect, request, Response
from werkzeug.utils import secure_filename
from urllib.parse         import quote

from . import checksum
from . import file
from . import httpL as http

from .app            import app, db, models
from .sampleresolver import SampleResolver
from .jsonify import jsonify


PART_EXT = "part"
CHECKSUM_EXT = "checksum"

# ------------------------------------------------------------ api routes --- #

@app.route('/')
def index():
    return redirect('/index.html', http.HTTP_302_FOUND)


@app.route('/test_user')
def test_user():
    if 'user' in session:
        return 'User: {}'.format(session['user'])
    else:
        return 'No user logged in'


@app.route('/projects', methods=['GET', 'POST'])
def get_projects():
    if request.method == 'GET':
        return jsonify(models.get_projects())
    else:
        if 'name' not in request.json:
            abort(Response('You must provide a name for the project', 400))
        if 'sample-mask' not in request.json:
            abort(Response('You must provide a sample mask for the project', 400))
        return jsonify(models.add_project(request.json['name'], request.json.get('sample-mask', '')))

@app.route('/admin/<dbitem>', methods=['GET'])
def all_admin_routes_get(dbitem):
    m = jsonify(models.all_admin_models_get(dbitem))
    return m


needed_fields = {'projects': ['name', 'sample_mask'],
                 'samples': ['name', 'project_id'],
                 'methods': ['name', 'description'],
                 'users': ['email', 'display_name'],
                 'groups': ['name', 'description'],
                 'group-users': ['group_id', 'user_id'],
                 'project-users': ['project_id', 'user_id', 'read', 'write'],
                 'project-groups': ['project_id', 'group_id', 'read', 'write']}
field_mapping = {'project': 'project_id',
                 'group': 'group_id',
                 'user': 'user_id',
                 'display-name': 'display_name',
                 'sample-mask': 'sample_mask'}
def sample_name_condition(req):
    sample_mask = models.get_project(obfuscated_id=as_id(req['project_id'])).sample_mask
    return re.fullmatch(sample_mask, req['name'])

def group_delete_check(id):
    g = models.all_admin_models_get_one('groups', id=id)
    return g.name != 'Admins'

extra_conditions = {'update/new': {'samples': {'name': sample_name_condition} }, 'delete': {'groups': group_delete_check} }

canDelete = ['groups', 'users', 'group-users', 'project-users', 'project-groups']
@app.route('/admin/<dbitem>/<id>', methods=['DELETE'])
def all_admin_routes_delete(dbitem, id):
    if dbitem in canDelete:
        if dbitem in extra_conditions['delete'] and not extra_conditions['delete'][dbitem](id):
            abort(Response(f'unable to delete from {dbitem}, doing so would break the database', 400))
        else:
            models.all_admin_models_delete(dbitem, id)
            return jsonify(models.all_admin_models_get(dbitem))
    else:
        abort(Response(f'unable to delete from {dbitem}', 400))



@app.route('/admin/<dbitem>/<obfuscated_id>', methods=['PUT'])
def all_admin_routes_put(dbitem, obfuscated_id):
    req = request.json
    for key in field_mapping:
        if key in req:
            req[field_mapping[key]] = req.pop(key)
    for field in needed_fields[dbitem]:
        if field not in req:
            abort(Response(f'You must provide a {field} for {dbitem}', 400))
    if dbitem in extra_conditions['update/new']:
        for field in needed_fields[dbitem]:
            if field in extra_conditions['update/new'][dbitem]:
                if not extra_conditions['update/new'][dbitem][field](req):
                    abort(Response(f'You must provide an valid {field} for the {dbitem}', 400))
    req.pop('id')
    return jsonify(models.all_admin_models_update(dbitem, as_id(obfuscated_id), req))

@app.route('/admin/<dbitem>', methods=['POST'])
def all_admin_routes_post(dbitem):
    req = request.json
    for key in field_mapping:
        if key in req:
            req[field_mapping[key]] = req.pop(key)
    for field in needed_fields[dbitem]:
        if field not in req and field != 'id':
            abort(Response(f'You must provide a {field} for {dbitem}', 400))
    if dbitem in extra_conditions['update/new']:
        for field in needed_fields[dbitem]:
            if field in extra_conditions['update/new'][dbitem]:
                if not extra_conditions['update/new'][dbitem][field](req):
                    abort(Response(f'You must provide an valid {field} for the {dbitem}', 400))
    return jsonify(models.all_admin_models_add(dbitem, req))


@app.route('/projects/<project>', methods=['GET', 'PUT'])
def get_project(project):
    if request.method == 'GET':
        return jsonify(models.get_project(obfuscated_id=as_id(project)))
    elif request.method == 'PUT':
        models.update_project(as_id(project), request.json)
        return jsonify(models.get_project(obfuscated_id=as_id(project)))

@app.route('/projects/<project>/samples/<sample>', methods=['GET', 'PUT'])
def get_project_sample(project, sample):
    sample_mask = models.get_project(obfuscated_id=as_id(project)).sample_mask
    if request.method == 'PUT':
        if 'name' not in request.json:
            abort(Response('You must provide a name for the sample', 400))
        if 'id' not in request.json:
            abort(Response('You must provide an id for the sample', 400))
        if not re.fullmatch(sample_mask, request.json['name']):
            abort(Response('You must provide an valid name for the sample', 400))
        models.update_sample(as_id(request.json['id']), request.json)
    return jsonify(
        models.get_project_sample(
            {'obfuscated_id': as_id(project)},
            {'obfuscated_id': as_id(sample)}))

@app.route('/methods', methods=['GET', 'POST'])
def existing_projects():
    if request.method == 'GET':
        return jsonify(models.get_methods())
    else:
        if 'name' not in request.json:
            abort(Response('You must provide a name for the project', 400))
        if 'description' not in request.json:
            abort(Response('You must provide a description for the project', 400))
        return jsonify(models.add_method(request.json['name'], request.json['description']))

@app.route('/methods/<method>', methods=['PUT'])
def add_method(method):
    models.update_method(as_id(method), request.json)
    return jsonify(models.get_method(obfuscated_id=as_id(method)))


@app.route('/users', methods=['GET', 'POST'])
def get_post_users():
    if request.method == 'GET':
        return jsonify(models.get_methods())
    else:
        if 'email' not in request.json:
            abort(Response('You must provide a email for the project', 400))
        if 'display_name' not in request.json:
            abort(Response('You must provide a display_name for the project', 400))
        return jsonify(models.add_method(request.json['email'], request.json['display_name']))

@app.route('/users/<user>', methods=['PUT'])
def add_user(user):
    models.update_user(as_id(user), request.json)
    return jsonify(models.get_user(obfuscated_id=as_id(method)))


@app.route('/projects/<project>/samples', methods=['GET', 'POST'])
def get_project_samples(project):
    # Allow clients to search for a sample rather than having to retrieve all
    # of the samples for a project (even if the latter is arguably the more
    # RESTful approach).  This enables a URL of the form:
    #
    #   http://.../projects/qwErt/samples?q=P001-B009-...
    #
    # In the absence of the query parameter we simply return the entire
    # collection.
    # FIXME: This should support pagination!
    search_terms = request.args.get('q')
    project_id   = as_id(project)
    if request.method == 'GET':
        if search_terms is not None:
            return jsonify(
                SampleResolver().resolve(search_terms.split(' '), project_id))
        else:
            return jsonify(
                models.get_samples(obfuscated_id=as_id(project_id)))
    else: # POST
        if 'name' not in request.json:
            abort(Response('You must provide a name for the sample', 400))
        sample_mask = models.get_project(obfuscated_id=as_id(project)).sample_mask
        if not re.fullmatch(sample_mask, request.json['name']):
            abort(Response('You must provide an valid name for the sample', 400))
        return jsonify(models.add_sample(project_id, request.json['name']))




@app.route('/projects/<_>/samples/<sample>/stages', methods=['GET'])
def get_project_sample_stages(_, sample):
    (stages, token) = models.get_sample_stages(as_id(sample))
    return jsonify({'sample' : sample,
                    'stages' : stages,
                    'token'  : token})


@app.route('/projects/<_>/samples/<sample>/stages/<stage>', methods=['GET'])
def get_project_sample_stage(_, sample, stage):
    return jsonify(models.get_sample_stages(as_id(sample), as_id(stage)))


@app.route('/projects/<project>/samples/<sample>/stages/<stage>', methods=['PUT'])
def put_project_sample_stage(project, sample, stage):
    method = models.get_method(obfuscated_id=as_id(request.json['method']))
    annotation = request.json['annotation']
    token = stage
    rsp = jsonify(
        models.add_sample_stage(
            as_id(sample), as_id(method.obfuscated_id), annotation, token))
    return (rsp, http.HTTP_201_CREATED)


@app.route('/projects/<project>/samples/<sample>/stages/<stage>/files', methods=['GET'])
def get_sample_stage_files(project, sample, stage):
    return jsonify({'stage-id' : stage,
                    'files'    : models.get_files(sample_stage_id=as_id(stage), status=None)})


@app.route('/methods/<method>', methods=['GET'])
def get_method(method):
    return jsonify(models.get_method(obfuscated_id=as_id(method)))


@app.route('/prepare-multipart-upload', methods=['POST'])
def prepare_file_upload():
    pass


@app.route('/upload-part', methods=['POST'])
def upload_file():
    part = request.files['file']

    # part_number = request.form['part-number']
    part_number = request.form['resumableChunkNumber']
    # total_number_parts = request.form['total-parts']
    total_number_parts = request.form['resumableTotalChunks']

    # file_identifier = request.form['upload-id']
    file_identifier = request.form['resumableIdentifier']
    part_upload_dir = upload_dir(file_identifier)
    mkdirp(part_upload_dir)

    # Save the part into a file with a name of the form 00.part.  The number of
    # leading zeroes depends on the number of parts.
    fmtstr = "%%0%dd" % len(total_number_parts)
    part_filename = '.'.join([(fmtstr % int(part_number)), PART_EXT])
    part_filepath = os.path.join(part_upload_dir, part_filename)
    part.save(part_filepath)

    return json.dumps(
        {'identifier': file_identifier,
         'part': part_number,
         'total_parts': total_number_parts})


@app.route('/complete-multipart-upload', methods=['POST'])
def complete_file_upload():
    request_data    = json.loads(request.data)
    file_identifier = request_data['upload-id']
    file_name       = secure_filename(request_data['file-name'])
    project         = request_data['project']
    sample          = request_data['sample']
    sample_stage    = as_id(request_data['sample-stage'])

    req_checksum_value  = request_data['checksum-value']
    req_checksum_method = request_data['checksum-method']

    part_upload_dir = upload_dir(file_identifier)
    reconstituted_file_name = os.path.join(part_upload_dir, file_name)
    parts = sorted(glob.glob(os.path.join(part_upload_dir, '*.part')))

    part_digester = checksum.get_digester(req_checksum_method)
    with open(reconstituted_file_name, 'wb') as target:
        def consume_part(data):
            target.write(data)
            part_digester.update(data)
        for part_name in parts:
            file.FileProcessor(part_name, consume_part).process()
    computed_checksum_value = part_digester.hexdigest()
    if computed_checksum_value != req_checksum_value:
        raise checksum.ChecksumMismatch(
            reconstituted_file_name,
            req_checksum_method,
            req_checksum_value,
            computed_checksum_value)

    with open('.'.join([reconstituted_file_name, CHECKSUM_EXT]), 'w') as cf:
        json.dump({'method': req_checksum_method,
                   'value' : req_checksum_value},
                  cf)

    logmsg = 'Successfully reconsitituted file: %s (%s=%s)' \
             % (reconstituted_file_name, req_checksum_method, req_checksum_value)
    app.logger.info(logmsg)
    models.add_file(
        os.path.relpath(
            reconstituted_file_name, app.config['UPLOAD_PATH']),
        sample_stage)

    return (json.dumps({'identifier' : file_identifier,
                        'file-name'  : file_name}),
            http.HTTP_202_ACCEPTED)

# -------------------------------------------------------- error handlers --- #

@app.errorhandler(checksum.ChecksumError)
def handle_unsupported_checksum_method(e):
    app.logger.error('Checksum error: %s' % e, exc_info=e)
    return (json.dumps({'status_code': e.status_code,
                        'message'    : e.message}),
            e.status_code)

# ------------------------------------------------------ data marshalling --- #

def as_id(resource_name):
    return resource_name.split('-')[0]

# ----------------------------------------------------------- utility fns --- #

def mkdirp(p):
    if os.path.exists(p) and os.path.isdir(p):
        return False
    else:
        os.makedirs(p)
        return True


def upload_dir(p):
    if isinstance(p, str):
        add_path = (p,) # Turn the single element into a tuple that can be
                        # joined into a full path
    elif isinstance(p, (list, tuple)):
        add_path = p
    else:
        msg = 'I Don\'t know how to create an upload directory path from "%s", %s' \
              % (p, type(p))
        raise Exception(msg)
    full_path = (app.config['UPLOAD_PATH'],) + add_path
    return os.path.join(*full_path)


def hashfile(fn, hasher, blocksize=65536):
    with open(fn, 'rb') as afile:
        buf = afile.read(blocksize)
        while len(buf) > 0:
            hasher.update(buf)
            buf = afile.read(blocksize)
    return hasher.hexdigest()


def hashstring(s, hasher):
    hasher.update(s)
    return hasher.hexdigest()
